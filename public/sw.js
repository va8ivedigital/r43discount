//console.log('ss');
var cachename = 'v1hddhd';
self.addEventListener('install', function(e) {
    console.log('Installed service worker');
 e.waitUntil(
   caches.open(cachename).then(function(cache) {
     return cache.addAll([
    
    '/build/js/all.js',
    '/build/css/main.css',
     ]);
   })
 );
});
self.addEventListener('fetch', function(event) {
   if ( event.request.url.match( '^.*(\/admin\/).*$' ) ) {
        return false;
    }
     // OR
    if ( event.request.url.indexOf( '/admin/' ) !== -1 ) {
        return false;
    }
 console.log(event.request.url);

 event.respondWith(
   caches.match(event.request).then(function(response) {
     return response || fetch(event.request);
   })
 );
});
self.addEventListener('activate', event => {
  console.log('Service Worker activating.');
});