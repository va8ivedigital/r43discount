<?php

namespace App\Http\Controllers\Api\V1\Web;

use App\Coupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CouponApiController extends Controller
{
    public function updateCouponViews(Request $request){
        try {
            $array = json_decode(file_get_contents('php://input'));

            if($array->id == '') {
                return response()->json('Id is required');
            }

            $id = $array->id;
            $info = Coupon::find($id);
            if($info){
                $views = $info->viewed+1;
                Coupon::where('id',$id)->update(['viewed' => $views]);
            }

            return response()->json(['message' => ''], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => $e->getMessage()], 403);
        }
    }
}
