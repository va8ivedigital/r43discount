<?php

namespace App\Http\Controllers\Api\V1\Web;

use App\Event;
use App\Http\Resources\Web\EventResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventsApiController extends Controller
{
    public function detail(){
        $data = [];
        try{
            $siteid = config('app.siteid');
            $dt = Carbon::now();
            $date = $dt->toDateString();

//            $data['detail'] = Event::select('id','title','short_description','long_description','meta_title','meta_keywords','meta_description')->with('categories')->with('stores')->with(['coupons'=> function($d) use ($date){
//                $d->select(['id','title','store_id','description','affiliate_url','custom_image_title','verified','sort','date_expiry','code','viewed'])->where('date_expiry', '>=', $date)->wherePublish(1);
//            } ])->with('coupons.store:id')->CustomWhereBasedData($siteid)->where('id',PAGE_ID)->first()->toArray();
            
    $data['detail'] = Event::select('id','title','short_description','long_description','meta_title','meta_keywords','meta_description')->with('categories')->with('stores')->with(['coupons'=> function($d) use ($date){$d->select(['id','title','store_id','description','affiliate_url','custom_image_title','verified','sort','date_expiry','code','viewed','free_shipping','exclusive'])->with('store.slugs')->where('date_expiry', '>=', $date)->wherePublish(1);
                } ])->with('coupons.store:id')->CustomWhereBasedData($siteid)->where('id',PAGE_ID)->first()->toArray();

            if($data['detail'] == null){
                return response()->json(['status' => $e->getMessage()], 403);
            }

            $data['dexist'] = true;
            return new EventResource($data);

        }catch (\Exception $e) {
            return response()->json(['status' => $e->getMessage()], 403);
        }
    }
}
