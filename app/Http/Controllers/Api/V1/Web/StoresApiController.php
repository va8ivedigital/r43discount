<?php

namespace App\Http\Controllers\Api\V1\Web;

use App\Blog;
use App\Category;
use App\Http\Resources\Web\StoreResource;
use App\Store;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoresApiController extends Controller
{
    public function __construct() {
        $this->store_model = new Store();
    }

    public function CustomWhereBasedData($query,$siteid=null) {
        return $query
            ->where('publish', 1)
            ->has('slugs')
            ->with(['slugs' => function($slugQuery){
                $slugQuery->select(['slug']);
            }])->with(['sites' => function($siteQuery){
                $siteQuery->select(['id']);
            }])
            ->whereHas('sites', function($q) use ($siteid){
                $q->where('site_id',$siteid);
            });
    }

    public function index(Request $request) {
        $data = [];
        try{
            $siteid = config('app.siteid');
            if($request->Input('q')){
                $stores = $this->getAllStores($siteid,$request->Input('q'));

            } else{
                $stores = $this->getAllStores($siteid);
            }

            $populars = $this->store_model->getPopularStores($siteid);

            $storeList = [];
            $popularList = [];

            foreach($stores as $store) {

                unset($store['sites']);
                unset($store['media']);

                $storeList[] =
                    array(
                        'id' => $store['id'],
                        'name' => $store['name'],
                        'short_description' => $store['short_description'],
                        'image_name' => isset($store['image']['name']) ? $store['image']['name'] : "",
                        'file_name' => isset($store['image']['file_name']) ? $store['image']['file_name'] : "",
                        'url' => isset($store['image']['url']) ? $store['image']['url'] : "",
                        'thumbnail' => isset($store['image']['thumbnail']) ? $store['image']['thumbnail'] : "",
                        'slug' => isset($store['slugs']['slug']) ? $store['slugs']['slug'] : "",
                    );
            }

            foreach($populars as $popular) {

                unset($store['sites']);

                $popularList[] =
                    array(
                        'id' => $popular['id'],
                        'name' => $popular['name'],
                        'short_description' => $popular['short_description'],
                        'image_name' => isset($popular['image']['name']) ? $popular['image']['name'] : "",
                        'file_name' => isset($popular['image']['file_name']) ? $popular['image']['file_name'] : "",
                        'url' => isset($popular['image']['url']) ? $popular['image']['url'] : "",
                        'thumbnail' => isset($popular['image']['thumbnail']) ? $popular['image']['thumbnail'] : "",
                        'slug' => isset($popular['slugs']['slug']) ? $popular['slugs']['slug'] : "",
                    );
            }
            $storeData['list'] = $storeList;
            $storeData['listCount'] = COUNT($stores);
            $storeData['popular'] = $popularList;

            $data['dexist'] = true;

            return new StoreResource($storeData);
        }catch (\Exception $e) {
            return response()->json(['status' => $e->getMessage()], 403);
        }

    }

    public function detail() {

        $data = [];

        try {

            $siteid = config('app.siteid');
            $dt = Carbon::now();
            $date = $dt->toDateString();

            $data['detail'] = Store::with(['storeCoupons', 'storesAddspaceStores'])->with(['storeCoupons'=> function($s) use ($date,$siteid){
                $s->where('date_expiry', '>=', $date)->CustomWhereBasedData($siteid);
            }])->with('categories')->CustomWhereBasedData($siteid)->where('id',PAGE_ID)->first()->toArray();

            $data['brand'] = Store::with('storeCoupons')->with('categories')->CustomWhereBasedData($siteid)->where('popular',1)->get()->toArray();

            $data['categories'] = Category::CustomWhereBasedData($siteid)->where('popular',1)->get()->toArray();

            $data['dexist'] = true;

            return new StoreResource($data);

        } catch (\Exception $e) {
            return response()->json(['status' => $e->getMessage()], 403);
        }

    }

    public function search(Request $request){
        $data = [];
        try{
            $_search_keyword = $request->search;
            $siteid = config('app.siteid');
            $stores = Store::where('name','LIKE','%'.$_search_keyword.'%')->CustomWhereBasedData($siteid)->get();
            if ($stores) {
                foreach ($stores as $k => $v) {
                    $data[$k]['title'] = $v->name;
                    $data[$k]['url'] = $v->slugs->slug;
                }
            }
            return new StoreResource($data);
        }catch (\Exception $e) {
            return response()->json(['status' => $e->getMessage()], 403);
        }
    }

    public function searchBlog(Request $request){
        $data = [];
        try{
            $_search_keyword = $request->search;
            $siteid = config('app.siteid');
            $blogs = Blog::where('title','LIKE','%'.$_search_keyword.'%')->CustomWhereBasedData($siteid)->get();

            if ($blogs) {
                foreach ($blogs as $k => $v) {
                    $data[$k]['title'] = $v->title;
                    $data[$k]['url'] = $v['slugs']['slug'];
                }
            }
            return new StoreResource($data);
        }catch (\Exception $e) {
            return response()->json(['status' => $e->getMessage()], 403);
        }
    }

    public function getAllStores($siteid, $q = '', $store_id = "") {
        $siteid = config('app.siteid');
        if($q != ''){
            if($q == '0-9'){
                $stores = $this->where(function($q1) use ($siteid){
                    $q1->where('name', 'LIKE', '0%')
                        ->orWhere('name', 'LIKE', '1%')
                        ->orWhere('name', 'LIKE', '2%')
                        ->orWhere('name', 'LIKE', '3%')
                        ->orWhere('name', 'LIKE', '4%')
                        ->orWhere('name', 'LIKE', '5%')
                        ->orWhere('name', 'LIKE', '6%')
                        ->orWhere('name', 'LIKE', '7%')
                        ->orWhere('name', 'LIKE', '8%')
                        ->orWhere('name', 'LIKE', '9%');
                })->CustomWhereBasedData($siteid)->orderBy('name', 'asc')->get()->toArray();
            }
            else{
                $stores = Store::select('id', 'name', 'short_description')->where('publish', 1)
                    ->has('slugs')
                    //->with(['slugs','sites'])
                    ->with(['slugs' => function($slugQuery){
                        $slugQuery->select(['obj_id','slug']);
                    }])
                    ->with('sites:id,name')
                    ->whereHas('sites', function($q) use ($siteid){
                        $q->where('site_id',$siteid);
                    })->where('name', 'LIKE', $q.'%')->orderBy('name', 'asc')->get()->toArray();
            }
        }
        else{
            $stores = Store::select('id', 'name', 'short_description')->where('publish', 1)
                ->has('slugs')
                //->with(['slugs','sites'])
                ->with(['slugs' => function($slugQuery){
                    $slugQuery->select(['obj_id','slug']);
                }])
                ->with('sites:id,name')
                ->whereHas('sites', function($q) use ($siteid){
                    $q->where('site_id',$siteid);
                })->orderBy('name', 'asc')->get()->toArray();
        }

        return $stores;
    }
}
