<?php

namespace App\Http\Controllers\Api\V1\Web;

use App\Banner;
use App\Category;
use App\Coupon;
use App\Event;
use App\Http\Controllers\Controller;
use App\Http\Resources\Web\HomeResource;
use App\Page;
use App\Site;
use App\Store;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeApiController extends Controller
{
    public function index() {
        $data = [];

        try {

            $siteid = config('app.siteid');
            $dt = Carbon::now();
            $date = $dt->toDateString();

            $data['banners'] = Banner::CustomWhereBasedData($siteid)->orderBy('sort', 'asc')->get()->toArray();
            $data['popularStores'] = Store::CustomWhereBasedData($siteid)->where('popular',1)->orderBy('name', 'asc')->get()->toArray();
            $query = Coupon::CustomWhereBasedData($siteid)->where('date_expiry', '>=', $date);
            $query = $query->where(function($q) {
                $q->orwhere('featured', 1)->orwhere('popular', 1);
            });
            $data['featuredCouponsAndPopularCoupons'] = $query->with('store.slugs')->orderBy('featured')->orderBy('title', 'asc')->get()->toArray();
            $data['recommended'] = Coupon::CustomWhereBasedData($siteid)->where('date_expiry', '>=', $date)->where('recommended',1)->with('store.slugs')->orderBy('title', 'asc')->take(6)->get()->toArray();
            $data['featuredCategories'] = Category::CustomWhereBasedData($siteid)->where('featured',1)->orderBy('title', 'asc')->get()->toArray();
            $data['sites'] = Site::select('id','name','country_name','country_code','url','html_tags','javascript_tags')->whereId($siteid)->get()->toArray();
            $data['social_icons'] = Site::select('instagram','twitter','facebook','youtube','pinterest','linked_in')->whereId(config('app.siteid'))->first()->toArray();

            $data['top_event'] = Event::select('id','title','slug')->with('slugs')->where('publish', 1)->where('top', 1)->with('sites')->whereHas('sites', function($q) use ($siteid) {
                $q->where('site_id',$siteid);
            })->orderBy('id', 'desc')->take(4)->get();

            $data['bottom_event'] = Event::select('id','title','slug')->with('slugs')->where('publish', 1)->where('bottom', 1)->with('sites')->whereHas('sites', function($q) use ($siteid) {
                $q->where('site_id',$siteid);
            })->orderBy('id', 'desc')->take(4)->get();

            $data['pages'] = Page::with('sites')->with('slugs')->whereHas('sites', function($page) use ($siteid){
                $page->where('site_id', $siteid);
            })->get();

            return new HomeResource($data);

        } catch (\Exception $e) {
            return response()->json(['status' => $e->getMessage()], 403);
        }
    }

    public function _404(){
        $data = [];
        $data['dexist'] = false;
        return response()->json(['data' => $data], 403);
    }
}
