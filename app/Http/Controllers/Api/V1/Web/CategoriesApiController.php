<?php

namespace App\Http\Controllers\Api\V1\Web;

use App\Category;
use App\Http\Resources\Web\CategoryResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesApiController extends Controller
{
    public function index() {
        $data = [];

        try {

            $siteid = config('app.siteid');
            $data['list'] = Category::select('id','title')->CustomWhereBasedData($siteid)->orderBy('title')->get()->toArray();
            $data['dexist'] = true;
            return new CategoryResource($data);

        } catch (\Exception $e) {
            return response()->json(['status' => $e->getMessage()], 403);
        }
    }

    public function detail() {

        $data = [];

        try {

            $siteid = config('app.siteid');

            $data['detail'] = Category::with(['categoryStores' => function ($q1) use ($siteid) {
                $q1->CustomWhereBasedData($siteid);
            }])->CustomWhereBasedData($siteid)->with('parentCategories')->where('id', PAGE_ID)->first();

            $data['popular'] = Category::CustomWhereBasedData($siteid)->orderBy('title')->get()->toArray();
            $data['dexist'] = true;
            return new CategoryResource($data);

        } catch (\Exception $e) {
            return response()->json(['status' => $e->getMessage()], 403);
        }

    }
}
