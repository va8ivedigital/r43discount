<?php

namespace App\Http\Controllers\Api\V1\Web;

use App\Blog;
use App\Category;
use App\Http\Resources\Web\BlogResource;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogsApiController extends Controller
{
    public function index() {
        $data = [];
        try{

            $siteid = config('app.siteid');
            if(!empty($_GET['category'])){
                $category = $_GET['category'];

                $data['blogIndiviual'] = Category::select('id','title','short_description')->CustomWhereBasedData($siteid)->whereHas('slugs',function($q) use ($category,$siteid){
                    $q->where('slug',$category)->where('site_id',$siteid);
                })->with(['blogs'=> function($q1){
                    $q1->with(['slugs','tags']);
                }])->first();

                $data['categoryList'] = Category::CustomWhereBasedData($siteid)->with(['blogs'=> function($q1){
                    $q1->with(['slugs','tags']);
                }])->select('id','title')->orderBy('title')->get()->toArray();

                $data['dexist'] = true;
                return new BlogResource($data);
            }

            $data['categoryList'] = Category::select('id','title')->CustomWhereBasedData($siteid)->with(['blogs'=> function($q1) use ($siteid){
                $q1->with('tags')->CustomWhereBasedData($siteid);
            }])->orderBy('title')->get()->toArray();

            $query = Blog::CustomWhereBasedData($siteid)->with(['categories' => function ($categoryQuery){
                $categoryQuery->with('slugs')->select('id','title');
            }] )->get();
            $data['topPostBlog'] = !$query->isEmpty() ? $query->random(1)->toArray() : $query->toArray();

            $data['trendingBlog'] = Blog::select('id','title')->CustomWhereBasedData($siteid)->orderBy('sort', 'asc')->take(3)->get()->toArray();

            $data['latestBlog'] = Blog::select('id','title')->with(['categories' => function ($categoryQuery){
                $categoryQuery->select('id','title');
            }] )->CustomWhereBasedData($siteid)->orderBy('id', 'desc')->take(4)->get()->toArray();

            $data['dexist'] = true;
            return new BlogResource($data);

        } catch (\Exception $e) {
            return response()->json(['status' => $e->getMessage()], 403);
        }
    }

    public function detail() {
        $data = [];
        try{
            $siteid = config('app.siteid');

            $data['list'] = Category::select('id','title')->CustomWhereBasedData($siteid)->with(['blogs'=> function($q1){
                $q1->with('slugs');
            }])->orderBy('title')->get()->toArray();

            $data['detail'] = Blog::CustomWhereBasedData($siteid)->with('tags')->with('categories.slugs')->with('user')->where('id',PAGE_ID)->first();

            if($data['detail']) $data['detail']=$data['detail']->toArray(); else abort(404);

            $data['trendingBlog'] = Blog::select('id','title')->CustomWhereBasedData($siteid)->orderBy('sort', 'asc')->take(3)->get()->toArray();

            $data['latestBlog'] = Blog::select('id','title')->CustomWhereBasedData($siteid)->with('categories')->orderBy('id', 'desc')->take(5)->get()->toArray();

            $data['blogWithCategory'] = Blog::select('id','title')->CustomWhereBasedData($siteid)->with(['categories' => function($categoryQuery){
                $categoryQuery->select(['id','title']);
            }])->take(3)->get()->toArray();
            $meta['title']=$data['detail'] ? $data['detail']['meta_title'] : '';
            $meta['keywords']=$data['detail'] ? $data['detail']['meta_keywords'] : '' ;
            $meta['description']=$data['detail'] ? $data['detail']['meta_description'] : '';
            $data['meta']=$meta;
            $data['dexist'] = true;

            return new BlogResource($data);
        }catch (\Exception $e) {
            return response()->json(['status' => $e->getMessage()], 403);
        }

    }

    public function load_data(Request $request){
        $siteid = config('app.siteid');
        if($request->ajax()){
            if($request['data_id'] > 0){
                $output = '';
                $last_id = '';
                $category_id = $request['category_id'];
                $data = Blog::CustomWhereBasedData($siteid)->where('id', '>', $request['data_id'])->orderBy('id', 'ASC')->limit(2)->get()->toArray();

                if(!empty($data)){
                    foreach($data as $record){
                        $url = config("app.app_path")."/".$record['slugs']['slug'] ;
                        $image = $record['image']['url'];
                        $output .= '
                  <div class="col-1 standard-post horizontal" >
                      <div class="inner">
                          <div class="post-image">
                              <a href="'.$url.'" class="image">
                                  <img src="'.$image.'" alt="">
                              </a>
                          </div>
                          <div class="post-details">
                              <a href="#">
                                  <div class="category-details">
                                      <div class="category-tags">
                                          <span >'.$record["title"].'</span>
                                      </div>
                                  </div>
                                  <div class="post-title">
                                      <h2>'.$record["short_description"].'</h2>
                                  </div>
                              </a>
                          </div>
                          <span class="btm-line"></span>
                      </div>
                  </div>
                  ';
                        $last_id = $record['id'];
                    }

                    $output .= '
             <div id="load_more">
              <button type="button" name="load_more_button" class="blgLoadMore form-control " blog-category-id="'.$category_id.'" data-id="'.$last_id.'" id="load_more_button">Load More</button>
             </div>
           ';

                    return new BlogResource($output);

                }else{

                    $output .= '
             <div id="load_more">
              <button type="button" name="load_more_button" class="blgLoadMore form-control">No More Blogs</button>
             </div>
           ';

                    return new BlogResource($output);

                }


            }else{

            }

        }
    }

    public function blogAuthor($slug){
        $data = [];
        $siteid = config('app.siteid');
        $data['pageCss'] = "blog_author";
        $user_id = User::where('name', $slug)->first()->id;
        // $data['list'] = Category::CustomWhereBasedData($siteid)->with('blogs.slugs')->orderBy('title')->get()->toArray();

        $data['list'] = Category::select('id','title')->CustomWhereBasedData($siteid)->with('blogs.slugs')->with(['blogs' => function($blogCustomWhereBasedDataQuery) use ($siteid){
            $blogCustomWhereBasedDataQuery->select('id','title')->CustomWhereBasedData($siteid);
        }])->orderBy('title')->get()->toArray();


        $data['blogListing'] = Blog::select('id','title','user_id')->CustomWhereBasedData($siteid)->with('user')->with(['categories' => function($categoryQuery) use ($siteid){
            $categoryQuery->select('id','title')->CustomWhereBasedData($siteid);
        }])->orderBy('id', 'DESC')->where('user_id', $user_id)->get()->toArray();

        $data['dexist'] = true;

        return new BlogResource($data);

    }

    public function authorLoadMoreData(Request $request){
        $siteid = config('app.siteid');
        if($request->ajax()){
            if($request['data_id'] > 0){

                $output = '';
                $last_id = '';

                $data = Blog::CustomWhereBasedData($siteid)->where('id', '<', $request['data_id'])->with('categories')->orderBy('id', 'DESC')->take(3)->get()->toArray();

                if(!empty($data)){
                    foreach($data as $blog){
                        $url = config("app.app_path")."/".$blog['slugs']['slug'] ;
                        $image = $blog['image']['url'];
                        $blogImage = $blog['categories'][0]['icon']['url'];
                        $blogCategoryTitle = $blog['categories'][0]['title'];
                        $blogTitle = $blog['title'];

                        $postTitle = substr($blog['title'], 0, 68);
                        $postTitleLength = strlen($blog['title']);

                        if($postTitleLength > 68){
                            $blogTitle = $postTitle." ... ";
                        }else{
                            $blogTitle = $blog['title'];
                        }

                        $output .= '
                  <div class="col-3 standard-post">
                    <div class="inner">
                        <div class="post-image">
                            <a href="'.$url.'" class="image">
                                <img src="'.$image.'" alt="">
                            </a>
                        </div>
                        <div class="post-details">
                            <a href="'.$url.'">
                                <div class="category-details">
                                    <span class="cat-icon">
                                        <img src="'.$blogImage.'" data-src="'.$blogImage.'">
                                    </span>
                                    <div class="category-title">
                                        <span>'.$blogCategoryTitle.'</span>
                                    </div>
                                </div>
                                <div class="post-title">
                                    <h2>'.$blogTitle.'</h2>
                                </div>
                            </a>
                            <span class="btm-line"></span>
                        </div>
                    </div>
                </div>
                  ';
                        $last_id = $blog['id'];
                    }

                    $output .= '
             <div id="load_more" style="width: 100%;">
              <button type="button" id="blog_load_more_button" blog-author-id="1" data-id="'.$last_id.'" class="blgLoadMore">LOAD MORE</button>
            </div>
           ';

                    return new BlogResource($output);

                }else{

                    $output .= '
             <div id="load_more" style="width: 100%;">
              <button type="button" name="blog_load_more_button" class="blgLoadMore form-control">No More Blogs</button>
             </div>
           ';

                    return new BlogResource($output);

                }

            }else{



            }

        }
    }
}
