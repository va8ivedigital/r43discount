<?php

namespace App\Http\Controllers\Api\V1\Web;

use App\Contact;
use App\Subscriber;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class ContactApiController extends Controller
{
    public function submitSubscribe( Request $request ) {
        try {

            $array = json_decode(file_get_contents('php://input'));

            if($array->email == '') {
                return response()->json('Email is required');
            }

            $email = $array->email;
            $model = new Subscriber;
            $checkSubscriber = $model->where('email', $email)->first();
            if($checkSubscriber){
                return response()->json(['message' => 'You are already subscribed'], 200);
            }
            $details = ip_details($array->ip);
            $model->email = $email;
            $model->page_link = url()->current();
            $loc = explode(',', $details['loc']);
            $model->longitude = $loc[0];
            $model->latitude = $loc[1];
            $model->country = $details['country'];
            $model->region = $details['region'];
            $model->city = $details['city'];
            $model->ip = get_client_ip();
            $model->client_agent = $request->server('HTTP_USER_AGENT');
            $model->site_id = config('app.siteid');
            $model->save();

            return response()->json(['message' => 'Thank You For Subscribing!'], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => $e->getMessage()], 403);
        }
    }

    public function contactStore(Request $request){
        try {
            $model = new Contact;

            $array = json_decode(file_get_contents('php://input'));

            $checkContactUser = $model->where('email', $array->email)->first();
            if($checkContactUser){

                return response()->json(['message' => 'You are already contact memeber! We will contact you soon.'], 200);

            }
            $model->name = $array->name;
            $model->email = $array->email;
            $model->contact = $array->contact;
            $model->subject = $array->subject;
            $model->message = $array->message;
            $model->save();

            return response()->json(['message' => 'Thanks for contacting us! We will get back to you shortly.'], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => $e->getMessage()], 403);
        }

    }
}
