<?php

namespace App\Http\Controllers\Api\V1\Web;

use App\Http\Resources\Web\PageResource;
use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesApiController extends Controller
{
    public function detail(){
        $data = [];

        try{
            $siteid = config('app.siteid');
            $data['pageRecord'] = Page::whereId(PAGE_ID)->CustomWhereBasedData($siteid)->first()->toArray();
            if($data['pageRecord'] == null){
                return response()->json(['status' => $e->getMessage()], 403);
            }
            $data['dexist'] = true;
            return new PageResource($data);

        }catch (\Exception $e) {
            return response()->json(['status' => $e->getMessage()], 403);
        }
    }
}
