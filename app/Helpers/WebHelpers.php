<?php
use App\Http\Controllers\Web\Routes as Routes;
use App\Site as SITES;
use DB as DB;
use Illuminate\Http\Request;

function clearCache() {
    \Artisan::call('config:clear');
    return true;
}

function checkCountryAndSetConfigs() {
    if (Schema::hasTable('sites')) {
        try {
            $segments = \Request::segments();
            $segments0 = isset($segments[0]) && $segments[0] == "web-api" ? $segments[1] ?? '' : $segments[0] ?? '';
            if ($segments0 == 'us') {
                config(['app.route_prefix' => 'us']);
            } else {
                config(['app.route_prefix' => $segments0]);
            }

            if($segments0=='admin'){
                return true;
            } else{

                $siteid = 0;$reg='us';
                $get = SITES::where('country_code',config('app.route_prefix'))->first();

                if($get){
                    $siteid = $get['id'];
                    $reg = $get['country_code'];

                    config(['app.db_obj' => $get]);
                }else{

                    $get = SITES::where('country_code','us')->first();
                    if($get) {
                        $siteid = $get['id'];
                        $reg = $get['country_code'];
                        config(['app.route_prefix'=>'us']);

                        config(['app.db_obj' => $get]);
                    } else {
                        abort(404);
                    }
                }

                config([
                    "app.siteid" => $siteid,
                    "app.Region" => $reg,
                    'app.namespace_name' => 'web',
                    'app.image_path' => \URL::to('/'),
                    'app.app_path' => \URL::to('/').'/'.config('app.route_prefix'),
                ]);

                clearCache();
                $routes = new Routes;
                if (isset($segments[0]) && $segments[0]=='web-api') {
                    $routes = $routes->find_route($segments[1] ?? '', $segments[2] ?? '', $segments[3] ?? '', $segments[4] ?? '');
                } else {
                    $routes = $routes->find_route($segments[0] ?? '', $segments[1] ?? '', $segments[2] ?? '', $segments[3] ?? '');
                }
                return true;

            }


        } catch (\Exception $e) {
            abort(404);
        }
    }
}

function data_toArray_Web($data) {
    $data = json_encode($data);
    return json_decode($data, true);
}


function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}

function get_string_between($string, $start, $end) {
    $string = " " . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) {
        return "";
    }

    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

// Function to get the client IP address
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    } else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    } else if (isset($_SERVER['HTTP_FORWARDED'])) {
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    } else if (isset($_SERVER['REMOTE_ADDR'])) {
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    } else {
        $ipaddress = 'UNKNOWN';
    }

    return $ipaddress;
}
function ip_details($IPaddress) {
    $json = file_get_contents("http://ipinfo.io/{$IPaddress}");
    $details = json_decode($json, true);
    return $details;
}
function getCouponRecord($coupon_id){
    return \App\Coupon::select('id','title','date_expiry','code','store_id')->with('store:id,name,store_url')->whereId($coupon_id)->first()->toArray();
}


?>
