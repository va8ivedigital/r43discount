<?php
Route::group(['prefix' => config('app.route_prefix'), 'as' => 'web_api.', 'namespace' => 'Api\V1\Web', 'middleware' => ['cors']], function () {
    // Home
    Route::get('/', 'HomeApiController@index');

    // Category
    Route::get('categories', 'CategoriesApiController@index');

    //for old url to new url redirect work
    if(defined('OLD_URL')){
        Route::get('/'.OLD_URL, function () {
            return redirect('/us/'.SLUG_LINK);
        });
    }

    Route::get('/blog', 'BlogsApiController@index')->name('blog');
    Route::get('/load-more-data', 'BlogsApiController@load_data');
    Route::get('/blog/author/{slug}', 'BlogsApiController@blogAuthor');
    Route::get('/author-load-more-data', 'BlogsApiController@authorLoadMoreData');

    Route::post('/update-coupon-views', 'CouponApiController@updateCouponViews');
    Route::post('/_subscribe', 'ContactApiController@submitSubscribe');
    Route::post('/contactStore', 'ContactApiController@contactStore');

    Route::get('/sitemap', 'StoresApiController@index');
    Route::get('/search_store', 'StoresApiController@search');
    Route::get('/search_blog', 'StoresApiController@searchBlog');


    //for old url to new url redirect work end
    if(defined('SLUG_LINK')) {
        Route::get(SLUG_LINK, SLUG_CONTROLLER . '@detail')->name(ROUTE_NAME);
    }else{
        Route::match(["get","post"], '/{parm1?}/{parm2?}/{parm3?}/{parm4?}', 'HomeApiController@_404')->name('FourOFour');
    }
});
